import Books from './Books.vue';
import showBook from './showBook.vue';

export default [
    { path: '/', component: Books},
    { path: '/show/:id', component: showBook}
]

