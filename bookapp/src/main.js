require('./assets/css/app.css')
import Vue from 'vue'
import App from './App.vue'
import VueResource from 'vue-resource'
import BootstrapVue from 'bootstrap-vue'
import VueRouter from 'vue-router'
import Routes from "./routes"


Vue.use(VueResource);
Vue.use(BootstrapVue);

const router = new VueRouter({
    routes: Routes,
    mode: 'history'
});



Vue.use(VueRouter);
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

new Vue({
  el: '#app',
  render: h => h(App),
  router: router
})
 