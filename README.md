

Preparing application
change database seting

/path/to/application/app/config.php 


Set document roots of your web server:

for frontend /path/to/application and using the URL http://book.test/
for backend /path/to/application/backend and using the URL http://backend.test/

go to bookapp folder
1) npm install
2) npm run dev

for auth backend
login: admin
password: admin

For nginx:

        server {
        charset utf-8;
        client_max_body_size 128M;

        listen 80;


        server_name book.test www.book.test;
        root /var/www/html/book.test;
        index index.php;


        location ~ \.css {
            root /var/www/html/book.test/frontend;
            expires max;
        }

        location ~* \.(jpg|jpeg|png|gif|ico|js|woff|woff2|ttf)$ {
            root /var/www/html/book.test/frontend;
            access_log off;
            expires max;
        }

        location ~* \.(js|css)$ {
            expires 1y;
            log_not_found off;
        }


        location / {
        # Redirect everything that isn't a real file to index.php
            rewrite ^/(.+)$ /index.php?url = $1;

            try_files $uri $uri/ /index.php$is_args$args;
        }




        location /phpmyadmin {
            root /usr/share/;
            index index.php;
            try_files $uri $uri/ = 404;

            location ~ ^/phpmyadmin/(doc|sql|setup)/ {
                deny all;
            }

            location ~ /phpmyadmin/(.+\.php)$ {
                fastcgi_pass unix:/run/php/php7.0-fpm.sock;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                include fastcgi_params;
                include snippets/fastcgi-php.conf;
            }
        }


        location ~ \.php$ {
            include snippets/fastcgi-php.conf;
            fastcgi_pass unix:/run/php/php7.0-fpm.sock;
            fastcgi_param SCRIPT_FILENAME $document_root/index.php;
        }

        location ~* /\. {
            deny all;
        }
		}

     
   
    server {
    charset utf-8;
    client_max_body_size 128M;

    listen 80; 
    server_name backend.test www.backend.test;
    root        /var/www/html/book.test/backend;
    index       index.php;

    auth_basic "Restricted";
    auth_basic_user_file /var/www/html/book.test/backend/htpasswd;


    location ~ \.css {
        root /var/www/html/book.test/backend;
        expires max;
    }

    location ~* \.(jpg|jpeg|png|gif|ico|js|woff|woff2|ttf)$ {
        root /var/www/html/book.test/frontend;
        access_log off;
        expires max;
    }

    location ~* \.(js|css)$ {
        expires 1y;
        log_not_found off;
    }


    location / {
        rewrite ^/(.+)$ /index.php?url=$1;

        try_files $uri $uri/ /index.php$is_args$args;
    }


    location /phpmyadmin {  
        root /usr/share/;
        index index.php;
        try_files $uri $uri/ =404;

        location ~ ^/phpmyadmin/(doc|sql|setup)/ {
        deny all;
    }

        location ~ /phpmyadmin/(.+\.php)$ {
            fastcgi_pass unix:/run/php/php7.0-fpm.sock;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            include fastcgi_params;
            include snippets/fastcgi-php.conf;
        }
    }

    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/run/php/php7.0-fpm.sock;
        fastcgi_param SCRIPT_FILENAME $document_root/index.php;
    }

    location ~* /\. {
        deny all;
    }
	}

Change the hosts file to point the domain to your server.

Windows: c:\Windows\System32\Drivers\etc\hosts
Linux: /etc/hosts
Add the following lines:

127.0.0.1   book.test
127.0.0.1   backend.test