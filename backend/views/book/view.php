<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <!-- Logo -->
                <div class="logo">
                    <h1><a>Admin Panel</a></h1>
                </div>
            </div>
            <div class="col-md-5">

            </div>
            <div class="col-md-2">

            </div>
        </div>
    </div>
</div>

<div class="page-content">
    <div class="row">
        <div class="col-md-2">
            <div class="sidebar content-box" style="display: block;">
                <?php include __DIR__.'/../layout/menu.php'; ?>
            </div>
        </div>
        <div class="col-md-10">


            <div class="row">
                <div class="col-md-12">

                    <div class="col-md-6">
                        <div class="content-box-large">
                            <div class="panel-heading">
                                <div class="panel-title">Book info</div>

                            </div>
                            <div class="panel-body">
                                <form class="form-horizontal" role="form" action="/book/update" method="post">
                                    <input type="text" hidden name="id" value="<?= $data['book']['id'] ?>">
                                    <div class="form-group">
                                        <label for="title_book" class="col-sm-2 control-label">Title</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="title" value="<?= $data['book']['title'] ?>" class="form-control" id="title_book" placeholder="">

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="price_book" class="col-sm-2 control-label">Price</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="price" value="<?= $data['book']['price'] ?>" class="form-control" id="price_book" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Textarea</label>
                                        <div class="col-sm-10">

                                            <textarea class="form-control" name="description"  placeholder="" rows="3"><?= $data['book']['description'] ?></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" class="btn btn-primary">Update</button>
                                        </div>
                                    </div>
                                </form>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Authors</label>
                                    <div class="col-sm-10">
                                        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>title</th>
                                                    <th>delete</th>	
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($data['authorsBook'] as $item): ?>
                                                    <tr class="odd gradeX">
                                                        <td><?= $item['title'] ?></td>

                                                        <td>
                                                            <form action = "/book/deleteAuthor" method="post">
                                                                <input hidden="" name="author_id" value="<?= $item['id'] ?>">
                                                                <input hidden="" name="book_id" value="<?= $data['book']['id'] ?>">
                                                                <button type="submit" name="submitDelete" class="btn btn-primary">Delete Author</button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Genres</label>
                                    <div class="col-sm-10">
                                        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>title</th>
                                                    <th>delete</th>	
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($data['genresBook'] as $item): ?>
                                                    <tr class="odd gradeX">
                                                        <td><?= $item['title'] ?></td>

                                                        <td>
                                                            <form action = "/book/deleteGenre" method="post">
                                                                <input hidden="" name="genre_id" value="<?= $item['id'] ?>">
                                                                <input hidden="" name="book_id" value="<?= $data['book']['id'] ?>">
                                                                <button type="submit" name="submitDelete" class="btn btn-primary">Delete Genre</button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>


                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="content-box-large">
                            <div class="panel-heading">
                                <div class="panel-title">Add authors</div>

                            </div>
                            <div class="panel-body">
                                <form class="form-horizontal" action="/book/author" role="form" method="post">
                                    <input hidden name="book_id" value="<?= $data['book']['id'] ?>">



                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Authors</label>
                                        <div class="col-sm-10">
                                            <select class="selectpicker" name="author_id[]" multiple>
                                                <?php if (is_array($data['authors'])): ?>
                                                    <?php foreach ($data['authors'] as $item): ?>
                                                        <option value="<?php echo $item['id']; ?>">
                                                            <?php echo $item['title']; ?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" class="btn btn-primary">Add</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="content-box-large">
                            <div class="panel-heading">
                                <div class="panel-title">Add genres</div>

                            </div>
                            <div class="panel-body">
                                <form class="form-horizontal" action="/book/genre" role="form" method="post">
                                    <input hidden name="book_id" value="<?= $data['book']['id'] ?>">



                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Genres</label>
                                        <div class="col-sm-10">
                                            <select class="selectpicker" name="genre_id[]" multiple>
                                                <?php if (is_array($data['allGenres'])): ?>
                                                    <?php foreach ($data['allGenres'] as $item): ?>
                                                        <option value="<?php echo $item['id']; ?>">
                                                            <?php echo $item['title']; ?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" class="btn btn-primary">Add</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>


                </div>

            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="content-box-large">
                        <div class="panel-heading">
                            <div class="panel-title">Bootstrap dataTables</div>
                        </div>
                        <div class="panel-body">
                                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
                                    <thead>
                                        <tr>
                                            <th>id</th>
                                            <th>title</th>
                                            <th>price</th>
                                            <th>description</th>
                                            <th>genre</th>
                                            <th>author</th>
                                            <th>view</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="odd gradeX">
                                            <td><?= $data['book']['id'] ?></td>
                                            <td><?= $data['book']['title'] ?></td>
                                            <td><?= $data['book']['price'] ?></td>
                                            <td><?= $data['book']['description'] ?></td>
                                            <td><?= $data['book']['genre_name'] ?></td>
                                            <td><?= $data['book']['author_name'] ?></td>
                                            <td>
                                               <form action="/book/delete" method="post">
                                                             <input type="hidden" name="id" value="<?= $data['book']['id'] ?>" >
                                                            <button type="submit" name="submitDelete" class="btn btn-primary">Delete</button>
                                                </form>
                                            </td>
                                        </tr>    
                                    </tbody>
                                </table>
                            
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>

