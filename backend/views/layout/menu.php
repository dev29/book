<ul class="nav">
    <li class="current"><a href="/"><i class="glyphicon glyphicon-home"></i> Admin</a></li>
    <li><a href="/genre"><i class="glyphicon glyphicon-calendar"></i> Genres</a></li>
    <li><a href="/author"><i class="glyphicon glyphicon-stats"></i> Authors</a></li>
    <li><a href="<?= FRONT_URL ?>"><i class="glyphicon glyphicon-stats"></i> Back to site</a></li>
</ul>