<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <!-- Logo -->
                <div class="logo">
                    <h1><a>Admin Panel</a></h1>
                </div>
            </div>
            <div class="col-md-5">
          
            </div>
            <div class="col-md-2">
             
            </div>
        </div>
    </div>
</div>

<div class="page-content">
    <div class="row">
        <div class="col-md-2">
            <div class="sidebar content-box" style="display: block;">
                <?php include __DIR__.'/../layout/menu.php';?>
            </div>
        </div>
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-6">
                   
                </div>

                <div class="col-md-6">
                        
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                   <div class="content-box-large">
                       <form class="form-horizontal" action="/genre/add" role="form" method="post">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="title" class="col-sm-3 control-label">Genre title </label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="title" class="form-control" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">

                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button type="submit" name="submit" class="btn btn-primary">Add</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
  				<div class="panel-heading">
					<div class="panel-title">Genre table</div>
				</div>
                       
  				<div class="panel-body">
                                   
                                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
						<thead>
							<tr>
								<th>id</th>
								<th>title</th>
                                                                <th>update</th>
                                                                <th>delete</th>
                                                                
							</tr>
						</thead>
						<tbody>
                                                    <?php foreach ($data['genre'] as $item): ?>
                                                	
                                                    <tr class="odd gradeX">
                                                        <form action="/genre/update" method="post">
                                                            <td><?= $item['id'] ?></td>
                                                        <td>
                                                            <input name="title" value="<?= $item['title'] ?>">
                                                            <input name="id" type="hidden" value="<?= $item['id'] ?>">
                                                        
                                                        </td>


                                                        <td>
                                                            <button type="submit" name="submitUpdate" class="btn btn-primary">Chenge</button>
                                                          
                                                        </td>
                                                       
                                                   
                                                     </form>
                                                <td>
                                                         <form action="/genre/delete" method="post">
                                                             <input type="hidden" name="id" value="<?=$item['id']?>" >
                                                            <button type="submit" name="submitDelete" class="btn btn-primary">Delete</button>
                                                         </form>
                                                        </td>
                                                     </tr>
                                                     <?php endforeach;?>
                                                     
						</tbody>
					</table>
                                       
                                    
  				</div>

  			</div>
                </div>
            </div>

            
        </div>
    </div>
</div>


