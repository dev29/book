<?php
use common\model\Db;

use common\model\Genre;

class GenreController extends BackController
{       
        /**
         * Display genre admin page 
         * 
         * @return mixed
         */
       	public function actionIndex()
	{
            
            $genres = Genre::start()->getAllGenres();

	    return $this->view('genre/index',['genre'=> $genres]);
	}
        
        /**
         * Add genre
         */
        public function actionAdd()
	{
        
            if(isset($_POST['submit'])){
                
                Genre::start()->addGenre($_POST);
                
            }
            
            header("Location:  ".$_SERVER['HTTP_REFERER']);
            
	}
        
        /**
         * Delete genre
         */
        public function actionDelete()
        {
             Genre::start()->deleteGenreById($_POST['id']);
             
             header("Location:  ".$_SERVER['HTTP_REFERER']);
        }
        
        /**
         * Update genre
         */
        public function actionUpdate()
        {
            
            if(isset($_POST['submitUpdate'])){
                
                Genre::start()->updateGenreById($_POST);
            
            }
            
            header("Location:  ".$_SERVER['HTTP_REFERER']);
            
        }



}
