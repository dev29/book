<?php

use common\model\{Book, Author, Genre};


class SiteController extends BackController
{       
        /**
         * Display admin index page
         * 
         * @return mixed
         */
	public function actionIndex()
	{
            $books = Book::start()->getBooksList();
            
            $allAuthors = Author::start()->getAllAuthors();
            
            $allGenres = Genre::start()->getAllGenres();
            
            return $this->view('site/index',[
                
                        'books'=> $books,
                        'allGenres'=>$allGenres,
                        'allAuthors'=>$allAuthors,
                    ]);
	}


}
