<?php
use common\model\Book;
use common\model\Author;
use common\model\Genre;

class BookController extends BackController
{
    /**
     * Display admin books page
     * 
     * @return mixed
     */
    public function actionIndex() 
    {
        $books = Book::start()->getBooksList();

        return $this->view('book/index', ['books' => $books]);
    }
    
    /**
     * Display admin book page
     * 
     * @return mixed
     */
    public function actionShow($id='')
    {
        $book = Book::start()->getBooksListById($id);
        $authorsBook = Author::start()->getAuthorsByBook($id);
        $genresBook = Genre::start()->getGenresByBook($id);

        $allAuthors = Author::start()->getAllAuthors();
        $allGenres = Genre::start()->getAllGenres();

        return $this->view('book/view', [
                    'book' => $book,
                    'authorsBook' => $authorsBook,
                    'authors' => $allAuthors,
                    'allGenres' => $allGenres,
                    'genresBook' => $genresBook
                ]);
    }
    
    /**
     * Add book
     */
    public function actionAdd()
    {
        Book::start()->addBook($_POST);
        header("Location:  ".$_SERVER['HTTP_REFERER']);
        
    }
    
    /**
     * Update book
     */
    public function actionUpdate()
    {
        Book::start()->updateBook($_POST);
        header("Location:  ".$_SERVER['HTTP_REFERER']);
        
    }
    
    /**
     * Delete book
     */
    public function actionDelete()
    {
        Book::start()->deleteBookById($_POST['id']);
        header("Location:  /admin");
        
    }
    
    /**
     * Add authors to book
     */
    public function actionAuthor()
    {
        Book::start()->addAuthorToBook($_POST);
        header("Location:  ".$_SERVER['HTTP_REFERER']);
        
    }
    
    /**
     * Delete author from book
     */
    public function actionDeleteAuthor()
    {
        Book::start()->deleteAuthorFromBook($_POST);
        header("Location:  ".$_SERVER['HTTP_REFERER']);
    }
    
    /**
     * Delete genre from book
     */
    public function actionDeleteGenre()
    {
        Book::start()->deleteGenreFromBook($_POST);
        header("Location:  ".$_SERVER['HTTP_REFERER']);
    }
    
    /**
     * Add genre to book
     */
    public function actionGenre()
    {
        Book::start()->addGenreToBook($_POST);
        header("Location:  ".$_SERVER['HTTP_REFERER']);
        
    }

    


}
