<?php

use common\model\Author;

class AuthorController extends BackController
{       

        /**
         * Display authors edit page
         * 
         * @return mixed
         */
	public function actionIndex()
	{
            
            $authors = Author::start()->getAllAuthors();

	    return $this->view('author/index',['authors'=> $authors]);
	}
        
        /**
         * Add author
         */
        public function actionAdd()
	{

            if(isset($_POST['submit'])){
                
                Author::start()->addAuthor($_POST);
            
            }
            
            header("Location:  ".$_SERVER['HTTP_REFERER']);
            
	}
        
        /**
         * Delete author
         */
        public function actionDelete()
        {
             Author::start()->deleteAuthorById($_POST['id']);
             
             header("Location:  ".$_SERVER['HTTP_REFERER']);
        }
        
        /**
         * Update author
         */
        public function actionUpdate()
        {
            
            if(isset($_POST['submitUpdate'])){
                
                Author::start()->updateAuthorById($_POST);
            
            }
            
            header("Location:  ".$_SERVER['HTTP_REFERER']);
            
        }



}
