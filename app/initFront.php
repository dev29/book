<?php

require_once (__DIR__.'/core/FrontApp.php');
require_once (__DIR__.'/core/FrontController.php');
require_once (__DIR__.'/config.php');

spl_autoload_register(function ($class) {
    $str = $class . '.php';
    $str = str_replace('\\', '/', $str);
    require_once $str;
});
