<?php

require_once (__DIR__.'/core/BackApp.php');
require_once (__DIR__.'/core/BackController.php');
require_once (__DIR__.'/config.php');

spl_autoload_register(function ($class) {
    $str =__DIR__.'\..\\' .$class . '.php';
    $str = str_replace('\\', '/', $str);
    require_once  $str;
});
