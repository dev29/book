<?php

class BackApp
{
    protected $controller = 'SiteController';
    
    protected $action = 'actionIndex';
    
    protected $params = [];
    

    public function __construct()
    {
        $url = $this->parceUrl();

        if(file_exists(__DIR__.'/../../backend/controllers/'.ucfirst($url[0]).'Controller.php')){
            
            $this->controller = ucfirst($url[0]).'Controller';
            unset($url[0]);
            
        }
        
        require_once __DIR__.'/../../backend/controllers/'.$this->controller.'.php';

        $this->controller = new $this->controller;
        
        if(isset($url[1])){
            if(method_exists($this->controller, 'action'. ucfirst($url[1]))){
                
                $this->action = 'action'.ucfirst($url[1]);
                unset($url[1]);
                
            }
        }
        
        $this->params = $url ? array_values($url) : [];
        
        call_user_func_array([$this->controller, $this->action], $this->params);

    }
    
    
    /**
     * @return array
     */
    public function parceUrl()
    {
        if(isset($_GET['url'])){
            return explode ('/',filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL));
        }
    }

}
