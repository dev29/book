<?php

class BackController
{
    public function view($view, $data = [])
    {
        require_once __DIR__.'/../../backend/views/layout/header.php';
        require_once __DIR__.'/../../backend/views/'.$view.'.php';
        require_once __DIR__.'/../../backend/views/layout/footer.php';
        
    }

}
