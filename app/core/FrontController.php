<?php

class FrontController
{
    public function view($view, $data = [])
    {
        require_once __DIR__.'/../../frontend/views/layout/header.php';
        require_once __DIR__.'/../../frontend/views/'.$view.'.php';
        require_once __DIR__.'/../../frontend/views/layout/footer.php';
        
    }

}
