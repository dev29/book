<?php

namespace common\model;

use app\model\Book;

class Email {

    private static $_instance = null;
    private $nameUser;
    private $emailUser;
    private $adressUser;
    private $phoneUser;
    private $copiesUser;
    private $options = [];
    private $massageForPeople;
    private $mail;
    private $subject;
    private $massage;


    /** 
     * @return common\model\Email
     */
    public static function start() 
    {
        if (null === self::$_instance) {
            self::$_instance = new self;
        }

        return self::$_instance;
    }
    
    /**
     * @param int $id
     * @param array $data
     * 
     * @return string
     */
    public function sendMail($id, $data) 
    {
        $this->nameUser = $_POST['name'];
        $this->emailUser = $_POST['email'];
        $this->adressUser = $_POST['address'];
        $this->phoneUser = $_POST['phone'];
        $this->copiesUser = $_POST['copies'];
        $this->options = array('options' => array('min_range' => 0, 'max_range' => 999999999999)
        );

        if (!filter_var($this->emailUser, FILTER_VALIDATE_EMAIL) or empty($this->emailUser)) {
            $this->massageForPeople = 'ENTER CORRECT YOUR EMAIL';
        } elseif (!filter_var($this->copiesUser, FILTER_VALIDATE_INT) or empty($this->copiesUser)) {
            $this->massageForPeople = 'ENTER COORECT NUMBER OF COPIES';
        } elseif (empty($this->nameUser)) {
            $this->massageForPeople = 'ENTER YOUR NAME, PLEASE';
        } elseif (empty($this->adressUser)) {
            $this->massageForPeople = 'ENTER YOUR ADRESS, PLEASE';
        } elseif (!filter_var($this->phoneUser, FILTER_VALIDATE_INT, $this->options) or empty($this->copiesUser) or strlen($this->phoneUser) != 12) {
            $this->massageForPeople = 'ENTER COORECT YOUR PHONE, for example: 380951234567';
        } else {

            $this->mail = 'sheva199.92@mail.ru';
            $this->subject = 'Order book';
            $this->massage = "Hi! My name is {$this->nameUser} My mail is: "
                             . "{$this->emailUser} My adress is: {$this->adressUser} "
                             . "My phone is: {$this->phoneUser} I want order book: "
                             . "{$data['title']} in an amount: {$_POST['copies']} copies";
            // if validation ok send mail
            if ( mail($this->mail, $this->subject, $this->massage)) {

                $this->massageForPeople = 'Order has been sent';
            } else {
                $this->massageForPeople = 'Order did not been sent becouse function mail din\'t work';
            }
        }
        return $this->massageForPeople;
    }

}
