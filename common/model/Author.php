<?php

namespace common\model;

use common\model\Db;
use PDO;

class Author
{
    private static $_instance = null;
    
    /** 
     * @return common\model\Author
     */
    public static function start()
    {
        if (null === self::$_instance)
        {
            self::$_instance = new self;
        }

        return self::$_instance;
    }
    
    /** 
     * @return array
     */
    public function getAllAuthors()
    {
        $db = DB::getInstance();
        
        $result = $db->query("SELECT * FROM authors");
        
        return $result->fetchAll(PDO::FETCH_ASSOC);
        
    }
    
    /**
     * @param int $id
     * 
     * @return array
     */
    public function getAuthorsByBook($id)
    {
        $db = Db::getInstance();

        $result = $db->query("
               SELECT authors.* FROM authors 
               JOIN book_to_author
               ON authors.id = book_to_author.author_id
               WHERE book_to_author.book_id = ".$id."
               GROUP BY authors.id
               ");

        $result->setFetchMode(PDO::FETCH_ASSOC);

        return $result->fetchAll();
    }


    /**
     * @param array $data
     * 
     * @return bool
     */
    public function addAuthor($data)
    {
        if (isset($data['title']) && !empty($data['title'])) {
            $db = DB::getInstance();

            $sql = 'INSERT INTO `authors` '
                    . '(title)'
                    . 'VALUES '
                    . '(:title)';

            $result = $db->prepare($sql);
            $result->bindParam(':title', $data['title'], PDO::PARAM_INT);

            return $result->execute();
            
        } 
    }
    
    
    /**
     * @param array $data
     * 
     * @return bool
     */
    public function updateAuthor($data)
    {
        
        $db = DB::getInstance();
        
        $sql = "UPDATE authors 
               SET 
               title=:title
               WHERE id = :id";
       
       $result = $db->prepare($sql);
       $result->bindParam(':id', $data['id'], PDO::PARAM_INT);
       $result->bindParam(':title', $data['title'], PDO::PARAM_STR);
      
       return $result->execute();
    }
    
    /**
     * @param int $id
     * 
     * @return bool
     */
    public function deleteAuthorById($id)
    {
       $db = Db::getInstance();
       
       $sql = 'DELETE FROM authors WHERE id= :id';
       
       $result = $db->prepare($sql);
       $result->bindParam('id', $id, PDO::PARAM_INT);
       
       return $result->execute();
    }
    
    /**
     * @param array $data
     * 
     * @return bool
     */
    public function updateAuthorById($data)
    {
        
        $db = DB::getInstance();

        $sql = "UPDATE authors 
               SET 
               title=:title
               WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $data['id'], PDO::PARAM_INT);
        $result->bindParam(':title', $data['title'], PDO::PARAM_STR);


        return $result->execute();
    }

}
