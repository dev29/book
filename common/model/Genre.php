<?php

namespace common\model;

use common\model\Db;
use PDO;

class Genre
{
    private static $_instance = null;
    
    /** 
     * @return common\model\Genre
     */
    public static function start()
    {
        if (null === self::$_instance)
        {
            self::$_instance = new self;
        }

        return self::$_instance;
    }
    
    /**
     * @return array
     */
    public function getAllGenres()
    {
        $db = DB::getInstance();
        
        $result = $db->query("SELECT * FROM genres");
        
        return $result->fetchAll(PDO::FETCH_ASSOC);
        
    }


    /**
     * @param array $data
     * 
     * @return bool
     */
    public function addGenre($data)
    {
        if (isset($data['title']) && !empty($data['title'])) {
            $db = DB::getInstance();

            $sql = 'INSERT INTO `genres` '
                    . '(title)'
                    . 'VALUES '
                    . '(:title)';

            $result = $db->prepare($sql);
            $result->bindParam(':title', $data['title'], PDO::PARAM_INT);

            return $result->execute();
            
        } else {
            
            return false;
        }
    }
    
    
    /**
     * @param array $data
     * 
     * @return bool
     */
    public function updateGenre($data)
    {
        
        $db = DB::getInstance();
        
        $sql = "UPDATE genres 
               SET 
               title=:title
               WHERE id = :id";
       
       $result = $db->prepare($sql);
       $result->bindParam(':id', $data['id'], PDO::PARAM_INT);
       $result->bindParam(':title', $data['title'], PDO::PARAM_STR);
      
       return $result->execute();
    }
    
    /**
     * @param int $id
     * 
     * @return bool
     */
    public function deleteGenreById($id)
    {
       $db = Db::getInstance();
       
       $sql = 'DELETE FROM genres WHERE id= :id';
       
       $result = $db->prepare($sql);
       $result->bindParam('id', $id, PDO::PARAM_INT);
       return $result->execute();
    }
    
    /**
     * @param array $data
     * 
     * @return bool
     */
    public function updateGenreById($data)
    {
        $db = DB::getInstance();

        $sql = "UPDATE genres 
               SET 
               title=:title
               WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $data['id'], PDO::PARAM_INT);
        $result->bindParam(':title', $data['title'], PDO::PARAM_STR);


        return $result->execute();
    }
    
    /**
     * @param int $id
     * 
     * @return array
     */
    public function getGenresByBook($id)
    {
        $db = Db::getInstance();

        $result = $db->query("
               SELECT genres.* FROM genres 
               JOIN book_to_genre
               ON genres.id = book_to_genre.genre_id
               WHERE book_to_genre.book_id = ".$id."
               GROUP BY genres.id
               ");

        $result->setFetchMode(PDO::FETCH_ASSOC);

        return $result->fetchAll();
    }

}
