<?php

namespace common\model;

use common\model\Db;
use PDO;

class Book
{
    private static $_instance = null;
    
    /** 
     * @return common\model\Book
     */
    public static function start()
    {
        if (null === self::$_instance)
        {
            self::$_instance = new self;
        }

        return self::$_instance;
    }
    
    /**
     * @return array
     */
    public function getAllBooks()
    {
        $db = DB::getInstance();
        
        $result = $db->query("SELECT * FROM books");
        
        return $result->fetchAll(PDO::FETCH_ASSOC);
        
    }
    
    /**
     * @return array
     */
    public function getBooksList()
    {
       $db = DB::getInstance();
        
       $booksList = array();
       
       $result = $db->query("SELECT books.*, GROUP_CONCAT(DISTINCT authors.title) AS author_name, 
					GROUP_CONCAT(DISTINCT authors.id) AS author_id, 
					GROUP_CONCAT(DISTINCT genres.title) AS genre_name, 
					GROUP_CONCAT(DISTINCT genres.id) AS genre_id 
					FROM books
            LEFT JOIN book_to_author ON book_to_author.book_id=books.id
            LEFT JOIN authors ON authors.id=book_to_author.author_id
            LEFT JOIN book_to_genre ON book_to_genre.book_id=books.id
            LEFT JOIN genres ON genres.id=book_to_genre.genre_id
             GROUP BY books.id
            ORDER BY books.id ASC
            ");
       
       $i=0;
       while($row = $result->fetch(PDO::FETCH_ASSOC)){
           $booksList[$i]['id'] = $row['id'];
           $booksList[$i]['title'] = $row['title'];
           $booksList[$i]['description'] = $row['description'];
           $booksList[$i]['price'] = $row['price'];
           $booksList[$i]['author_name'] = str_replace(',', ', ', $row['author_name']);
           $booksList[$i]['genre_name'] = str_replace(',', ', ', $row['genre_name']);
     
           $i++;
       }
       

       return $booksList;
    }
    
    
    /**
     * @param int $id
     * 
     * @return array
     */
    public function getBooksListById($id)
    {
       $db = DB::getInstance();
        
       $booksList = array();
       
       $result = $db->query("SELECT books.*, GROUP_CONCAT(DISTINCT authors.title) AS author_name, 
					GROUP_CONCAT(DISTINCT authors.id) AS author_id, 
					GROUP_CONCAT(DISTINCT genres.title) AS genre_name, 
					GROUP_CONCAT(DISTINCT genres.id) AS genre_id 
					FROM books
            LEFT JOIN book_to_author ON book_to_author.book_id=books.id
            LEFT JOIN authors ON authors.id=book_to_author.author_id
            LEFT JOIN book_to_genre ON book_to_genre.book_id=books.id
            LEFT JOIN genres ON genres.id=book_to_genre.genre_id
            WHERE books.id =".$id."
            GROUP BY books.id
            ORDER BY books.id ASC
            ");
       
       return $result->fetch(PDO::FETCH_ASSOC);
       
    }
    
    
    /**
     * @param int $id
     * 
     * @return array
     */
    
    public function getBooksListByGenre($id)
    {
       $db = DB::getInstance();
        
       $booksList = array();
       
       $result = $db->query("SELECT books.*, GROUP_CONCAT(DISTINCT authors.title) AS author_name, 
					GROUP_CONCAT(DISTINCT authors.id) AS author_id, 
					GROUP_CONCAT(DISTINCT genres.title) AS genre_name, 
					GROUP_CONCAT(DISTINCT genres.id) AS genre_id 
					FROM books
            LEFT JOIN book_to_author ON book_to_author.book_id=books.id
            LEFT JOIN authors ON authors.id=book_to_author.author_id
            LEFT JOIN book_to_genre ON book_to_genre.book_id=books.id
            LEFT JOIN genres ON genres.id=book_to_genre.genre_id
            WHERE genres.id =".$id."
             GROUP BY books.id
            ORDER BY books.id ASC
            ");
       
       $i=0;
       while($row = $result->fetch(PDO::FETCH_ASSOC)){
           $booksList[$i]['id'] = $row['id'];
           $booksList[$i]['title'] = $row['title'];
           $booksList[$i]['description'] = $row['description'];
           $booksList[$i]['price'] = $row['price'];
           $booksList[$i]['author_name'] = str_replace(',', ', ', $row['author_name']);
           $booksList[$i]['genre_name'] = str_replace(',', ', ', $row['genre_name']);
     
           $i++;
       }
       

       return $booksList;
    }
    
    
    /**
     * @param int $id
     * 
     * @return array
     */
    
    public function getBooksListByAuthor($id)
    {
       $db = DB::getInstance();
        
       $booksList = array();
       
       $result = $db->query("SELECT books.*, GROUP_CONCAT(DISTINCT authors.title) AS author_name, 
					GROUP_CONCAT(DISTINCT authors.id) AS author_id, 
					GROUP_CONCAT(DISTINCT genres.title) AS genre_name, 
					GROUP_CONCAT(DISTINCT genres.id) AS genre_id 
					FROM books
            LEFT JOIN book_to_author ON book_to_author.book_id=books.id
            LEFT JOIN authors ON authors.id=book_to_author.author_id
            LEFT JOIN book_to_genre ON book_to_genre.book_id=books.id
            LEFT JOIN genres ON genres.id=book_to_genre.genre_id
            WHERE authors.id =".$id."
            GROUP BY books.id
            ORDER BY books.id ASC
            ");
       
       $i=0;
       while($row = $result->fetch()){
           $booksList[$i]['id'] = $row['id'];
           $booksList[$i]['title'] = $row['title'];
           $booksList[$i]['description'] = $row['description'];
           $booksList[$i]['price'] = $row['price'];
           $booksList[$i]['author_name'] = str_replace(',', ', ', $row['author_name']);
           $booksList[$i]['genre_name'] = str_replace(',', ', ', $row['genre_name']);
     
           $i++;
       }
       

       return $booksList;
    }

    /**
     * @param int $id
     * 
     * @return array
     */

    public function getBookById($id)
    {
        $db = DB::getInstance();
        
        $result = $db->query("SELECT * FROM books WHERE id = $id");
        
        return $result->fetch(PDO::FETCH_ASSOC);
        
    }
    
    /**
     * @param array $data
     * 
     * @return bool
     */
    
    public function addAuthorToBook($data)
    {

        $db = DB::getInstance();
        
        foreach ($data['author_id'] as $item){
            $sql = 'INSERT INTO `book_to_author` '
                    . '(book_id, author_id)'
                    . 'VALUES '
                    . '(:book_id, :author_id)';

            $result = $db->prepare($sql);
            $result->bindParam(':book_id', $data['book_id'], PDO::PARAM_INT);
            $result->bindParam(':author_id', $item, PDO::PARAM_INT);
            $result->execute();
        }
        
        return $result->execute();

    }
    
    /**
     * @param array $data
     * 
     * @return bool
     */
    
    public function deleteAuthorFromBook($data)
    {
        $db = DB::getInstance();

        $sql = 'DELETE FROM book_to_author WHERE book_id= :book_id AND author_id = :author_id';

        $result = $db->prepare($sql);
        $result->bindParam(':book_id', $data['book_id'], PDO::PARAM_INT);
        $result->bindParam(':author_id', $data['author_id'], PDO::PARAM_INT);
        
        return $result->execute();
    }
    
    /**
     * @param array $data
     * 
     * @return bool
     */
    
    public function deleteGenreFromBook($data)
    {
        $db = DB::getInstance();

        $sql = 'DELETE FROM book_to_genre WHERE book_id= :book_id AND genre_id = :genre_id';

        $result = $db->prepare($sql);
        $result->bindParam(':book_id', $data['book_id'], PDO::PARAM_INT);
        $result->bindParam(':genre_id', $data['genre_id'], PDO::PARAM_INT);
        
        return $result->execute();
    }
    
    /**
     * @param array $data
     * 
     * @return bool
     */

    public function addGenreToBook($data)
    {

        $db = DB::getInstance();
        
        foreach ($data['genre_id'] as $item){
            $sql = 'INSERT INTO `book_to_genre` '
                    . '(book_id, genre_id)'
                    . 'VALUES '
                    . '(:book_id, :genre_id)';

            $result = $db->prepare($sql);
            $result->bindParam(':book_id', $data['book_id'], PDO::PARAM_INT);
            $result->bindParam(':genre_id', $item, PDO::PARAM_INT);
            $result->execute();
        }

    }


    /**
     * @param array $data
     * 
     * @return bool
     */
    
    public function addBook($data)
    {
        $db = DB::getInstance();

        $sql = "INSERT INTO `books` 
               (title, description, price)
               VALUES
               (:title, :description, :price)
        ";

        $result = $db->prepare($sql);
        $result->bindParam(':title', $data['title'], PDO::PARAM_STR);
        $result->bindParam(':description', $data['description'], PDO::PARAM_STR);
        $result->bindParam(':price', $data['price'], PDO::PARAM_STR);

        if($result->execute() == true){
            $book_id = $db->lastInsertId();
            
            foreach ($data['genre_id'] as $item){
            $sql = 'INSERT INTO `book_to_genre` '
                    . '(book_id, genre_id)'
                    . 'VALUES '
                    . '(:book_id, :genre_id)';

            $result = $db->prepare($sql);
            $result->bindParam(':book_id', $book_id, PDO::PARAM_INT);
            $result->bindParam(':genre_id', $item, PDO::PARAM_INT);
            $result->execute();
        }
        
        
        foreach ($data['author_id'] as $item){
            $sql = 'INSERT INTO `book_to_author` '
                    . '(book_id, author_id)'
                    . 'VALUES '
                    . '(:book_id, :author_id)';

            $result = $db->prepare($sql);
            $result->bindParam(':book_id', $book_id, PDO::PARAM_INT);
            $result->bindParam(':author_id', $item, PDO::PARAM_INT);
            $result->execute();
        }
        
            
            
        }
        
        
        return $result->execute();
    }
    
    /**
     * @param array $data
     * 
     * @return bool
     */
    
    public function updateBook($data)
    {
        $db = DB::getInstance();

        $sql = "UPDATE books 
               SET 
               title=:title,
               description=:description,
               price=:price
               WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $data['id'], PDO::PARAM_INT);
        $result->bindParam(':title', $data['title'], PDO::PARAM_STR);
        $result->bindParam(':description', $data['description'], PDO::PARAM_STR);
        $result->bindParam(':price', $data['price'], PDO::PARAM_STR);


        return $result->execute();
    }
    
    /**
     * @param int $id
     * 
     * @return bool
     */
    
    public function deleteBookById($id)
    {
       $db = Db::getInstance();
       
       $sql = 'DELETE FROM books WHERE id= :id';
       
       $result = $db->prepare($sql);
       $result->bindParam('id', $id, PDO::PARAM_INT);
       return $result->execute();
    }


}
