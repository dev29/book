<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <!-- Logo -->
                <div class="logo">
                    <h1><a>Book katalog</a></h1>
                </div>
            </div>
            <div class="col-md-5">

            </div>
            <div class="col-md-2">

            </div>
        </div>
    </div>
</div>

<div class="col-md-12">

    <div class="col-md-2">
        <div class="sidebar content-box" style="display: block;">
            <div id="genres">
                <ul class="nav">
                    <!-- Main menu -->
                    <li class="current"><a href="<?= BACK_URL ?>"><i class="glyphicon glyphicon-home"></i> Admin</a></li>
                    <li class="submenu open">
                        <a href><i class="glyphicon glyphicon-calendar"></i> Genres</a>
                        <ul class="nav ">

                            <li   v-for="(item, i) in listOfGenres"   >
                                <a v-bind:data-genre-id="item.id" v-on:click="onClick('genre'+i)"  v-bind:ref="'genre'+i" >{{ item.title }}</a>
                            </li>


                        </ul>
                    </li>


                </ul> 
            </div>

            <div id = "authors">
                <ul class="nav">
                    <li class="submenu open">
                        <a href><i class="glyphicon glyphicon-calendar"></i> Authors</a>
                        <ul class="nav ">
                            
                             <li   v-for="(item, i) in listOfAuthors"   >
                                <a v-bind:data-author-id="item.id" v-on:click="onClick('author'+i)"  v-bind:ref="'author'+i" >{{ item.title }}</a>
                            </li>
                            
                            

                        </ul>
                    </li>
                </ul>
            </div>

        </div>
    </div>
    <div class="col-md-10">
        <div class="container">    
            <div class="row">
                <div id="book-app">
                    <div class="col-md-5" v-for="item in listOfBooks">				
                        <div class="panel panel-default  panel--styled">
                            <div class="panel-body">
                                <div class="col-md-12 panelTop">	
                                    <div class="col-md-4">	
                                        <a href="/site/view/">
                                            <img class="img-responsive" src="http://placehold.it/350x550" alt=""/>
                                        </a>
                                    </div>
                                    <div class="col-md-8">	
                                        <h2><a href="/site/view/">{{ item.title }}</a></h2>
                                        <p>{{ item.description }}</p>
                                    </div>
                                </div>

                                <div class="col-md-12">

                                    <div class="col-md-12 text-left">
                                        <h5><b>Price:</b> <span class="">${{ item.price }}</span></h5>
                                    </div>
                                    <div class="col-md-12 text-left">
                                        <h5><b>Jenres:</b> <span class="">{{ item.genre_name }}</span></h5>
                                    </div>
                                    <div class="col-md-12 text-left">
                                        <h5><b>Authors:</b> <span class="">{{ item.author_name }}</span></h5>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

</div>