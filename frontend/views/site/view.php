<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <!-- Logo -->
                <div class="logo">
                    <h1><a>Book katalog</a></h1>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12">

    <div class="col-md-2">
        <div class="sidebar content-box" style="display: block;">
            <ul class="nav">
                <!-- Main menu -->
                <li class="current"><a href="<?= BACK_URL ?>"><i class="glyphicon glyphicon-home"></i> Admin</a></li>
                <li class="submenu open">
                    <a href><i class="glyphicon glyphicon-calendar"></i> Genres</a>
                    <ul class="nav ">
                        <?php foreach ($data['genres'] as $item): ?>
                            <li><a href="/site/genre/<?= $item['id'] ?>"><?= $item['title'] ?></a></li>
                        <?php endforeach; ?>

                    </ul>
                </li>
                <li class="submenu open">
                    <a href><i class="glyphicon glyphicon-calendar"></i> Authors</a>
                    <ul class="nav ">
                        <?php foreach ($data['authors'] as $item): ?>
                            <li><a href="/site/author/<?= $item['id'] ?>"><?= $item['title'] ?></a></li>
                        <?php endforeach; ?>

                    </ul>
                </li>

            </ul>            
        </div>
    </div>
    <div class="col-md-10">
        <div class="container">    
            <div class="row">

                <div class="col-md-12">				
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-md-12">	
                                <div class="col-md-4">	

                                    <img class="img-responsive" src="http://placehold.it/350x550" alt=""/>

                                </div>
                                <div class="col-md-8">	
                                    <h2><?= $data['books']['title'] ?></h2>
                                    <p><?= $data['books']['description'] ?></p>
                                    <h5><b>Jenres:</b> <span class=""><?= $data['books']['genre_name'] ?></span></h5>
                                    <h5><b>Authors:</b> <span class=""><?= $data['books']['author_name'] ?></span></h5>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-12 text-left">
                                    <h5><b>Price:</b> <span class="">$<?= $data['books']['price'] ?></span></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-7 ">
                    <?php if (isset($data['message'])) echo $data['message']; ?>
                    <div class="content-box-large">
                        <div class="panel-heading">
                            <div class="panel-title">Order the book</div>

                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" action="/site/order/<?= $data['books']['id'] ?>" role="form" method="post">
                                <div class="form-group">
                                    <label for="name_order" class="col-sm-3 control-label">Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="name" id="name_order" placeholder="Frank">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Email</label>
                                    <div class="col-sm-9">
                                        <input type="email" name="email" class="form-control" id="inputEmail3" placeholder="Email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="address" class="col-sm-3 control-label">Address</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="address" id="address" placeholder="Londond etc.">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="phone" class="col-sm-3 control-label">Phone</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="phone" id="phone" placeholder="380505050555">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="copies" class="col-sm-3 control-label">Number of books</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="copies" id="copies" placeholder="3">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button type="submit" class="btn btn-primary">Order</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>



    </div>



</div>
