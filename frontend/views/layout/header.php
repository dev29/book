<!DOCTYPE html>
<html>
    <head>

        <title>Тестовая страница</title>
        <meta charset="utf-8" />
        <link href="/css/bootstrap.min.css" rel="stylesheet">
        <link href="/css/styles.css" rel="stylesheet" media="all">
        <link href="/css/bootstrap-select.min.css" rel="stylesheet" media="all">
        <link href="/css/app.css" rel="stylesheet" media="all">
        <script src="/js/vue.js"></script>
        <script src="/js/vue-resource.js"></script>
        <script src="/js/vue-router.js"></script>


    </head>
    <body>
<!--             <header>
        <div class="header-wrapper">
          <img src="http://acmelogos.com/images/logo-8.svg" alt="" class="logo">

          <nav>
            <router-link to="/about">+ Бренды</router-link>
            <router-link to="/blog" active-class="grey">+ Блог</router-link>
            <router-link to="/projects">+ Проекты</router-link>
            <router-link to="/contacts">+ Контакты</router-link>
          </nav>
        </div>
      </header>-->

<!--<div id="my-app">
  <h1>Hello App!</h1>
  <p>

    <router-link to="/foo/:1">Go to Foo</router-link>
    <router-link to="/bar">Go to Bar</router-link>
  </p>
   route outlet 
   component matched by the route will render here 
  <router-view></router-view>
</div>-->