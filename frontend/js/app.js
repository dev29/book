//
//
//const Foo = { template: '<div class="panel panel-default  panel--styled"><div class="panel-body"><div class="col-md-12 panelTop"><div class="col-md-4"><a href="/site/view/"><img src="http://placehold.it/350x550" alt="" class="img-responsive"></a></div> <div class="col-md-8"><h2><a href="/site/view/">First title booke</a></h2> <p>first description bo4</p></div></div> <div class="col-md-12"><div class="col-md-12 text-left"><h5><b>Price:</b> <span>$102</span></h5></div> <div class="col-md-12 text-left"><h5><b>Jenres:</b> <span> Fantasy, Action Adventure, Science Fiction, Speculative Fiction</span></h5></div> <div class="col-md-12 text-left"><h5><b>Authors:</b> <span>Bella Forrest, Harold Schechter, J. K. Rowling, Loretta Nyhan</span></h5></div></div></div></div>' }
//const Bar = { template: '<div v-on:click="goBack">bar</div>',
//    
//      computed: {
//    username () {
//        console.log(333);
//      // Мы скоро разберём что такое `params`
//      return this.$route.params.username
//    }
//  },
//  methods: {
//    goBack () {
//        console.log(4444);
//      window.history.length > 1
//        ? this.$router.go(-1)
//        : this.$router.push('/')
//    }
//  }
//
//
//
//}
//
//// 2. Define some routes
//// Each route should map to a component. The "component" can
//// either be an actual component constructor created via
//// Vue.extend(), or just a component options object.
//// We'll talk about nested routes later.
//const routes = [
//  { path: '/foo/:id', component: Foo },
//  { path: '/bar', component: Bar }
//]
//
//// 3. Create the router instance and pass the `routes` option
//// You can pass in additional options here, but let's
//// keep it simple for now.
//const router = new VueRouter({
//  routes
//})
//
//// 4. Create and mount the root instance.
//// Make sure to inject the router with the router option to make the
//// whole app router-aware.
//const app2 = new Vue({
//  router
//}).$mount('#my-app')


//var Brands = require('./views/Brands.vue');
//var Contacts = require('./views/Contacts.vue');
//var Projects = require('./views/Projects.vue');
//var Blog = require('./views/Blog.vue');
//var Post = require('./views/Post.vue');
//
//Vue.use(VueRouter);
//
//var router = new VueRouter({
//  routes: [
//    { path: '/brands', component: Brands },
//    { path: '/contacts', component: Contacts },
//    { path: '/projects', component: Projects },
//    { path: '/blog', component: Blog },
//    { path: '/post/:id', name: 'post', component: Post }
//  ]
//});
//
//new Vue({
//  el: '#app',
//  router: router
//});





var app = new Vue({
    el: '#book-app',
    data: {

        listOfBooks: [],

    },
    methods: {

        fetchData: function (pageNumber) {

            this.$http.get('/site/getBooks')
                    .then(function (response) {

                        app.listOfBooks = response.body;

                    })
                    .catch(function (error) {
                        console.log(error);
                    });
        }
    },
    created: function () {
        this.fetchData();
    }
});

var genres = new Vue({
    el: '#genres',
    data: {
        listOfGenres: [],
        id: '',
    },
    methods: {
        onClick: function (genre) {
            var genre_id = this.$refs[genre][0].dataset.genreId;
            this.getBooksByGenre(genre_id);
           
        },
        fetchData: function (pageNumber) {
            this.$http.get('/site/getGenres')
                    .then(function (response) {
                       
                        genres.listOfGenres = response.body;
                       
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
        },

        getBooksByGenre: function (id) {

            this.$http.get('/site/getBookByGenreId/' + id)
                    .then(function (response) {
                        app.listOfBooks = response.body;
                    })
                    .catch(function (error) {
                        console.log(error);
                    });


        }
    },
    created: function () {
        this.fetchData(0);
    }
});



var authors = new Vue({
    el: '#authors',
    data: {
        listOfAuthors: [],
        numberOfPages: 0,
        max: 5
    },
    methods: {
        onClick: function (author) {
            var author_id = this.$refs[author][0].dataset.authorId;
            this.getBooksByAuthor(author_id);
           
        },
        fetchData: function (pageNumber) {
            this.$http.get('/site/getAuthors', {
                params: {
                    pageNumber: pageNumber,
                    max: this.max
                }
            })
                    .then(function (response) {
                        console.log(response);
                        authors.listOfAuthors = response.body;
                        console.log(authors.listOfAuthors);
                        authors.numberOfPages = response.data.numberOfPages;
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
        },
        
        getBooksByAuthor: function (id) {

            this.$http.get('/site/getBookByAuthorId/' + id)
                    .then(function (response) {
                        app.listOfBooks = response.body;
                    })
                    .catch(function (error) {
                        console.log(error);
                    });


        }
    },
    created: function () {
        this.fetchData(0);
    }
});














//
//
//
////Vue.component('app-page', {
//template: '#campaignBlock',
//
//data: function() {
//    return{
//        campaigns: []
//    }
//  },
//
//ready: function () {
//    this.fetchCampaigns();
//},
//
//methods: {
//    fetchCampaigns: function () {
//      var campaigns = [];
//      this.$http.get('/site/getBooks')
//        .success(function (campaigns) {
//          this.$set('campaigns', campaigns);
//
//        })
//        .error(function (err) {
//          campaigns.log(err);
//        });
//    },
//}
//});
//
//var bookApp = new Vue({
//    el: "#book-app2",
//     created(){
//      this.getBooks();  
//    },
//    
//    methods: {
//        
//        getBooks: function(){
//            console.log('test');
//            this.$http.get('/site/getBooks').then(function(data){
//            
//            console.log(data);
//             this.books = data.body;
//              
//              console.log(this.books);
//            });
//        },
//        post: function(){
//          this.$http.post('/site/genre/2', {
//              title: "first titel",
//              id: 13
//              
//          }).then(function(data){
//              console.log(data);
//          });  
//        },
//        
//        getBook: function(){
//            this.$http.get('/site/genre/2').then(function(data){
//                console.log(data);
//            });
//        },
//        addToA: function () {
//            console.log('a');
//            return  this.a + this.age;
//        },
//
//        addToB: function () {
//            console.log('b');
//            return this.b + this.age;
//        },
//
//    },
//    data:{
//        books: []
//    },
//    computed: {
//        compClasses: function () {
//            return {
//                available: this.available,
//                nearby: this.nearby
//            }
//        }
//
//    },
//   
//});
//
//
//
//
//
//
//
//
//
//
////
////
////
////
////
////
////
////Vue.component('greeting', {
////    template: '<p>Hey there, I am re-usabel {{name}} component <button v-on:click=changeName ></button>',
////    data: function () {
////        return {
////            name: "joi"
////        }
////    },
////    methods: {
////        changeName: function () {
////            this.name = 'Mario';
////        }
////    }
////});
////
////
////var one = new Vue({
////    el: "#book-app",
////    data: {
////        name: 'Ivan',
////        characters: ['Mario', 'Battle', 'Juk'],
////        ninjas: [
////            {name: 'Joe', age: 23},
////            {name: 'Joe1', age: 25},
////            {name: 'Joe2', age: 21},
////            {name: 'Joe3', age: 15},
////        ],
////        error: false,
////        success: false,
////        available: false,
////        nearby: false,
////        message: "Hello world!",
////        name: 'Ivan',
////        website: 'http://www.mail.cz',
////        websiteTag: '<a href="/test">TEst2 </a>',
////        age: 25,
////        x: 0,
////        y: 0,
////        a: 0,
////        b: 0,
////    },
////    methods: {
////        addToA: function () {
////            console.log('a');
////            return  this.a + this.age;
////        },
////
////        addToB: function () {
////            console.log('b');
////            return this.b + this.age;
////        },
////
////        greet: function (time) {
////            return 'Good ' + time + ' ' + this.name;
////        },
////
////        add: function (years) {
////            return   this.age += years;
////        },
////
////        subtract: function (years) {
////            return this.age -= years;
////        },
////
////        updateXY: function (event) {
////            console.log(event);
////            this.x = event.offsetX;
////
////            this.y = event.offsetY;
////        },
////        click: function () {
////            alert('You clicked me');
////        }
////
////    },
////    computed: {
////        compClasses: function () {
////            return {
////                available: this.available,
////                nearby: this.nearby
////            }
////        }
//////         addToA: function(){
//////             console.log('a');
//////         return  this.a + this.age;  
//////        },
//////        
//////        addToB: function(){
//////            console.log('b');
//////         return this.b + this.age;  
//////        },
////    }
////})
////
////
////
