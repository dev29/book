<?php

use common\model\{
    Book,
    Genre,
    Author,
    Email
};

class SiteController extends FrontController {


    public function actionIndex() 
    {
        echo json_encode(Book::start()->getBooksList());

        echo json_encode(Genre::start()->getAllGenres());

        echo json_encode(Author::start()->getAllAuthors());
    }

    public function actionGetBooks() 
    {
        echo json_encode(Book::start()->getBooksList());
    }

    public function actionGetGenres() 
    {
        echo json_encode(Genre::start()->getAllGenres());
    }

    public function actionGetAuthors() 
    {
        echo json_encode(Author::start()->getAllAuthors());
    }

    public function actionGetBookByGenreId($id) 
    {

        echo json_encode(Book::start()->getBooksListByGenre($id));
    }

    public function actionGetBookByAuthorId($id) 
    {

        echo json_encode(Book::start()->getBooksListByAuthor($id));
    }

    public function actionGetBookById($id) 
    {
        echo json_encode(Book::start()->getBooksListById($id));
    }

    public function actionOrder($id) 
    {

        $book = Book::start()->getBookById($id);

        $message = Email::start()->sendMail($id, $book);

        echo $message;
    }

}
